import { BrowserRouter } from "react-router-dom";
import { HelmetProvider } from "react-helmet-async";

import Router from "./routes";
import ScrollToTop from "./components/common/ScrollToTop";
import ThemeProvider from "./theme";
import RTL from "./utils/RTL";

const App = () => (
  <HelmetProvider>
    <BrowserRouter>
      <ThemeProvider>
        <RTL>
          <ScrollToTop />
          <Router />
        </RTL>
      </ThemeProvider>
    </BrowserRouter>
  </HelmetProvider>
);

export default App;
