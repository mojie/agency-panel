import { Helmet } from "react-helmet-async";
import { styled } from "@mui/material/styles";
import { Link as RouterLink } from "react-router-dom";
import { Button, Typography, Container, Box } from "@mui/material";

const StyledContent = styled("div")(({ theme }) => ({
  maxWidth: 480,
  margin: "auto",
  minHeight: "100vh",
  display: "flex",
  justifyContent: "center",
  flexDirection: "column",
  padding: theme.spacing(12, 0),
}));

const Page404 = () => (
  <>
    <Helmet>
      <title> Seirapp | 404 Page Not Found </title>
    </Helmet>

    <Container>
      <StyledContent sx={{ textAlign: "center", alignItems: "center" }}>
        <Typography variant="h3" paragraph>
          متاسفیم، صفحه‌ای یافت نشد!
        </Typography>

        <Box
          component="img"
          src="/images/illustration_404.svg"
          sx={{ height: 260, mx: "auto", my: { xs: 5, sm: 10 } }}
        />

        <Button to="/" size="large" variant="contained" component={RouterLink}>
          بازگشت به صفحه‌اصلی
        </Button>
      </StyledContent>
    </Container>
  </>
);
export default Page404;
