import { Helmet } from "react-helmet-async";
import { alpha, styled } from "@mui/material/styles";
import { Grid, Container, Typography, Card } from "@mui/material";

import Iconify from "../common/Iconify";

const StyledIcon = styled("div")(({ theme }) => ({
  margin: "auto",
  display: "flex",
  borderRadius: "50%",
  alignItems: "center",
  width: theme.spacing(8),
  height: theme.spacing(8),
  justifyContent: "center",
  marginBottom: theme.spacing(3),
}));

const HomePage = () => (
  <>
    <Helmet>
      <title> seirapp </title>
    </Helmet>

    <Container maxWidth="xl">
      <Typography variant="h4" sx={{ mb: 5 }}>
        وضعیت بیمه ها
      </Typography>

      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <Card
            sx={{
              py: 5,
              boxShadow: 0,
              textAlign: "center",
              color: (theme) => theme.palette.primary.darker,
              bgcolor: (theme) => theme.palette.primary.lighter,
            }}
          >
            <StyledIcon
              sx={{
                color: (theme) => theme.palette.primary.dark,
                backgroundImage: (theme) =>
                  `linear-gradient(135deg, ${alpha(
                    theme.palette.primary.dark,
                    0
                  )} 0%, ${alpha(theme.palette.primary.dark, 0.24)} 100%)`,
              }}
            >
              <Iconify
                icon={"ant-design:apple-filled"}
                width={24}
                height={24}
              />
            </StyledIcon>

            <Typography variant="h3">{(201).toLocaleString()}</Typography>

            <Typography
              variant="subtitle2"
              sx={{ opacity: 0.72 }}
              fontSize={24}
              mt={2}
            >
              تعداد سفرها
            </Typography>
          </Card>
        </Grid>

        <Grid item xs={12} sm={6}>
          <Card
            sx={{
              py: 5,
              boxShadow: 0,
              textAlign: "center",
              color: (theme) => theme.palette.info.darker,
              bgcolor: (theme) => theme.palette.info.lighter,
            }}
          >
            <StyledIcon
              sx={{
                color: (theme) => theme.palette.info.dark,
                backgroundImage: (theme) =>
                  `linear-gradient(135deg, ${alpha(
                    theme.palette.info.dark,
                    0
                  )} 0%, ${alpha(theme.palette.info.dark, 0.24)} 100%)`,
              }}
            >
              <Iconify
                icon={"ant-design:apple-filled"}
                width={24}
                height={24}
              />
            </StyledIcon>

            <Typography variant="h3">{(456258).toLocaleString()}</Typography>

            <Typography
              variant="subtitle2"
              sx={{ opacity: 0.72 }}
              fontSize={24}
              mt={2}
            >
              تعداد مسافران
            </Typography>
          </Card>
        </Grid>

        <Grid item xs={12} sm={6}>
          <Card
            sx={{
              py: 5,
              boxShadow: 0,
              textAlign: "center",
              color: (theme) => theme.palette.warning.darker,
              bgcolor: (theme) => theme.palette.warning.lighter,
            }}
          >
            <StyledIcon
              sx={{
                color: (theme) => theme.palette.warning.dark,
                backgroundImage: (theme) =>
                  `linear-gradient(135deg, ${alpha(
                    theme.palette.warning.dark,
                    0
                  )} 0%, ${alpha(theme.palette.warning.dark, 0.24)} 100%)`,
              }}
            >
              <Iconify
                icon={"ant-design:apple-filled"}
                width={24}
                height={24}
              />
            </StyledIcon>

            <Typography variant="h3">{(125486987).toLocaleString()}</Typography>

            <Typography
              variant="subtitle2"
              sx={{ opacity: 0.72 }}
              fontSize={24}
              mt={2}
            >
              کل مبلغ بیمه ( تومان )
            </Typography>
          </Card>
        </Grid>

        <Grid item xs={12} sm={6}>
          <Card
            sx={{
              py: 5,
              boxShadow: 0,
              textAlign: "center",
              color: (theme) => theme.palette.error.darker,
              bgcolor: (theme) => theme.palette.error.lighter,
            }}
          >
            <StyledIcon
              sx={{
                color: (theme) => theme.palette.error.dark,
                backgroundImage: (theme) =>
                  `linear-gradient(135deg, ${alpha(
                    theme.palette.error.dark,
                    0
                  )} 0%, ${alpha(theme.palette.error.dark, 0.24)} 100%)`,
              }}
            >
              <Iconify
                icon={"ant-design:apple-filled"}
                width={24}
                height={24}
              />
            </StyledIcon>

            <Typography variant="h3">{(15254325).toLocaleString()}</Typography>

            <Typography
              variant="subtitle2"
              sx={{ opacity: 0.72 }}
              fontSize={24}
              mt={2}
            >
              مانده حساب ( تومان )
            </Typography>
          </Card>
        </Grid>
      </Grid>
    </Container>
  </>
);
export default HomePage;
