import { useState } from "react";
import { Helmet } from "react-helmet-async";
import { GiConfirmed } from "react-icons/gi";
import { RiFileExcel2Fill } from "react-icons/ri";
import {
  Card,
  Table,
  Stack,
  Avatar,
  Button,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  IconButton,
  TableContainer,
  TablePagination,
  TableHead,
} from "@mui/material";

import Scrollbar from "../common/Scrollbar";
import tours from "../../_mock/tours";

const ToursPage = () => {
  const [page, setPage] = useState(0);

  const [rowsPerPage, setRowsPerPage] = useState(5);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  return (
    <>
      <Helmet>
        <title> Seirapp | Tours </title>
      </Helmet>

      <Container maxWidth="xl">
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          mb={5}
        >
          <Typography variant="h4" gutterBottom>
            درخواست‌ها
          </Typography>
          <Button
            onClick={() => alert("همه درخواست‌ها تایید شد")}
            variant="contained"
          >
            تایید همه
          </Button>
        </Stack>

        <Card>
          <Scrollbar>
            <TableContainer sx={{ minWidth: 800 }}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>آژانس برگزارکننده</TableCell>
                    <TableCell>عنوان</TableCell>
                    <TableCell>تعداد روز</TableCell>
                    <TableCell>تاریخ اجرا</TableCell>
                    <TableCell>موقعیت مکانی</TableCell>
                    <TableCell>وضعیت</TableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {tours
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => {
                      const {
                        id,
                        agency,
                        title,
                        days,
                        date,
                        location,
                        status,
                      } = row;

                      return (
                        <TableRow hover key={id}>
                          <TableCell>
                            <Stack
                              direction="row"
                              alignItems="center"
                              spacing={2}
                            >
                              <Avatar alt={agency.name} src={agency.avatar} />
                              <Typography>{agency.name}</Typography>
                            </Stack>
                          </TableCell>
                          <TableCell>{title}</TableCell>
                          <TableCell>{days}</TableCell>
                          <TableCell>{date}</TableCell>
                          <TableCell>{location}</TableCell>
                          <TableCell>
                            {status ? (
                              <IconButton
                                size="large"
                                color="inherit"
                                onClick={() => alert("clicked")}
                              >
                                <GiConfirmed color="#00ff0f" />
                              </IconButton>
                            ) : (
                              <>
                                <IconButton
                                  size="large"
                                  color="inherit"
                                  onClick={() => alert("clicked")}
                                >
                                  <GiConfirmed color="#ff0000" />
                                </IconButton>

                                <IconButton
                                  size="large"
                                  color="inherit"
                                  onClick={() => alert("clicked")}
                                >
                                  <RiFileExcel2Fill color="#336e33" />
                                </IconButton>
                              </>
                            )}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
          </Scrollbar>

          <TablePagination
            labelRowsPerPage={"تعداد سطر"}
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={tours.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
    </>
  );
};

export default ToursPage;
