import PropTypes from "prop-types";
import { useEffect } from "react";
import { styled, alpha } from "@mui/material/styles";
import { useLocation, NavLink as RouterLink } from "react-router-dom";

import {
  Box,
  Link,
  Drawer,
  Typography,
  Avatar,
  List,
  ListItemText,
  ListItemIcon,
  ListItemButton,
} from "@mui/material";

import Logo from "../../common/Logo";
import account from "../../../_mock/account";
import Scrollbar from "../../common/Scrollbar";
import useResponsive from "../../hooks/useResponsive";
import SvgColor from "../../common/SvgColor";

const NAV_WIDTH = 280;

const StyledAccount = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  padding: theme.spacing(2, 2.5),
  borderRadius: Number(theme.shape.borderRadius) * 1.5,
  backgroundColor: alpha(theme.palette.grey[500], 0.12),
}));

const StyledNavItem = styled((props) => (
  <ListItemButton disableGutters {...props} />
))(({ theme }) => ({
  ...theme.typography.body2,
  height: 48,
  position: "relative",
  textTransform: "capitalize",
  color: theme.palette.text.secondary,
  borderRadius: theme.shape.borderRadius,
}));

const StyledNavItemIcon = styled(ListItemIcon)({
  width: 22,
  height: 22,
  color: "inherit",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

const Nav = ({ openNav, onCloseNav }) => {
  const { pathname } = useLocation();

  const isDesktop = useResponsive("up", "lg");

  useEffect(() => {
    if (openNav) {
      onCloseNav();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pathname]);

  const renderContent = (
    <Scrollbar
      Scrollbar
      sx={{
        height: 1,
        "& .simplebar-content": {
          height: 1,
          display: "flex",
          flexDirection: "column",
        },
      }}
    >
      <Box sx={{ px: 2.5, py: 3, display: "inline-flex" }}>
        <Logo />
      </Box>

      <Box sx={{ mb: 5, mx: 2.5 }}>
        <Link underline="none">
          <StyledAccount>
            <Avatar src={account.photoURL} alt="photoURL" />

            <Box sx={{ ml: 2 }}>
              <Typography variant="subtitle2" sx={{ color: "text.primary" }}>
                {account.displayName}
              </Typography>

              <Typography variant="body2" sx={{ color: "text.secondary" }}>
                {account.role}
              </Typography>
            </Box>
          </StyledAccount>
        </Link>
      </Box>

      <Box>
        <List disablePadding sx={{ p: 1 }}>
          <StyledNavItem
            component={RouterLink}
            to={"/dashboard/app"}
            sx={{
              "&.active": {
                color: "text.primary",
                bgcolor: "action.selected",
                fontWeight: "fontWeightBold",
              },
            }}
          >
            <StyledNavItemIcon>
              <SvgColor
                src={`/icons/ic_analytics.svg`}
                sx={{ width: 1, height: 1 }}
              />
            </StyledNavItemIcon>

            <ListItemText disableTypography primary={"مدیریت داشبورد"} />
          </StyledNavItem>

          <StyledNavItem
            component={RouterLink}
            to={"/dashboard/tours"}
            sx={{
              "&.active": {
                color: "text.primary",
                bgcolor: "action.selected",
                fontWeight: "fontWeightBold",
              },
            }}
          >
            <StyledNavItemIcon>
              <SvgColor
                src={`/icons/ic_user.svg`}
                sx={{ width: 1, height: 1 }}
              />
            </StyledNavItemIcon>

            <ListItemText disableTypography primary={"مدیریت سفرها"} />
          </StyledNavItem>
        </List>
      </Box>
    </Scrollbar>
  );

  return (
    <Box
      component="nav"
      sx={{ flexShrink: { lg: 0 }, width: { lg: NAV_WIDTH } }}
    >
      {isDesktop ? (
        <Drawer
          open
          variant="permanent"
          PaperProps={{
            sx: {
              width: NAV_WIDTH,
              bgcolor: "background.default",
              borderRightStyle: "dashed",
            },
          }}
        >
          {renderContent}
        </Drawer>
      ) : (
        <Drawer
          open={openNav}
          onClose={onCloseNav}
          ModalProps={{ keepMounted: true }}
          PaperProps={{ sx: { width: NAV_WIDTH } }}
        >
          {renderContent}
        </Drawer>
      )}
    </Box>
  );
};

Nav.propTypes = {
  openNav: PropTypes.bool,
  onCloseNav: PropTypes.func,
};

export default Nav;
