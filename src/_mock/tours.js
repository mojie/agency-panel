const tours = [
  {
    id: "1",
    agency: {
      name: "آیلارگشت پارسیان",
      avatar: "/images/avatar_2.jpg",
    },
    title: "ییلاقات سوباتان",
    days: "۳",
    date: "۱۹ الی ۲۲ فروردین ۱۴۰۱",
    location: "گیلان - تالش",
    status: false,
  },
  {
    id: "2",
    agency: {
      name: "اسپلیت البرز",
      avatar: "/images/avatar_3.jpg",
    },
    title: "دماوند",
    days: "۲",
    date: "۲۱ الی ۲۳ فروردین ۱۴۰۱",
    location: "تهران",
    status: false,
  },
  {
    id: "3",
    agency: {
      name: "تال پرواز",
      avatar: "/images/avatar_4.jpg",
    },
    title: "باتومی",
    days: "۶",
    date: "۱۸ الی ۲۴ فروردین ۱۴۰۱",
    location: "ترکیه-باتومی",
    status: true,
  },
];

export default tours;
