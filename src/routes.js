import { Navigate, useRoutes } from "react-router-dom";

import SimpleLayout from "./components/layouts/SimpleLayout";
import DashboardLayout from "./components/layouts/DashboardLayout";

import ToursPage from "./components/pages/ToursPage";
import LoginPage from "./components/pages/LoginPage";
import Page404 from "./components/pages/Page404";
import HomePage from "./components/pages/HomePage";

const Router = () => {
  const routes = useRoutes([
    {
      path: "/dashboard",
      element: <DashboardLayout />,
      children: [
        { element: <Navigate to="/dashboard/app" />, index: true },
        { path: "app", element: <HomePage /> },
        { path: "tours", element: <ToursPage /> },
      ],
    },
    {
      path: "login",
      element: <LoginPage />,
    },
    {
      element: <SimpleLayout />,
      children: [
        { element: <Navigate to="/dashboard/app" />, index: true },
        { path: "404", element: <Page404 /> },
        { path: "*", element: <Navigate to="/404" /> },
      ],
    },
    {
      path: "*",
      element: <Navigate to="/404" replace />,
    },
  ]);

  return routes;
};

export default Router;
